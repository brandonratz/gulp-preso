/* File: gulpfile.js */

// grab our gulp packages
var gulp  = require('gulp'),
    gutil = require('gulp-util');

// create a default task and just log a message
gulp.task('default', function() {
  return gutil.log('Gulp is running!')
});

// new task - gulp mytask
gulp.task('mytask', function() {
  //do stuff
  return gutil.log('Gulp is running the task: ', this.tasks.mytask.name);
});

gulp.task('dependenttask', ['mytask'], function() {
  //do stuff after 'mytask' is done.
  return gutil.log('Gulp can now run the task: ', this.tasks.dependenttask.name);
});
